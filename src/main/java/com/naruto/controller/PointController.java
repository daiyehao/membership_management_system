package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.PointMapper;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.PointEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.response.PointsResp;
import com.naruto.service.PointService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "积分模块")
@RequestMapping("points")

public class PointController {
	@Autowired
	PointMapper pointMapper;
	
	@Autowired
	PointService pointService;
	
    @ApiOperation(value="会员积分列表", notes="根据pageNum进行分页查询")
    @RequestMapping(value={"/memberPointList"}, method=RequestMethod.POST)
	public String memberPointList(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum) {
    	
    	 PageHelper.startPage(pageNum,5);
         List<PointEntity> blogList = pointMapper.selectAll();
         PageInfo<PointEntity> pageInfo = new PageInfo<PointEntity>(blogList);
    	
		System.out.println(pageInfo.toString());
		String json =JSON.toJSONString(pageInfo);
		System.out.println(json);
		return json;
	}
    @ApiOperation(value="会员积分详情", notes="根据ID进行查询")
    @RequestMapping(value={"/memberPointDetail"}, method=RequestMethod.POST)
    public String memberPointDetail(@RequestParam(value = "id",defaultValue = "1")Integer id) {
    	Example example =new Example(PointEntity.class);
    	example.createCriteria().andEqualTo("id",id);
    	PointEntity  entity = pointMapper.selectOneByExample(example);
		System.out.println(entity.toString());
		String json =JSON.toJSONString(entity);
		System.out.println(json);
		return json;
	}
    
    @ApiOperation(value="会员积分删除", notes="根据ID进行删除")
    @RequestMapping(value={"/memberPointDel"}, method=RequestMethod.POST)
    public String memberPointDel(@RequestParam(value = "id")Integer id) {
    	Example example =new Example(PointEntity.class);
    	example.createCriteria().andEqualTo("id",id);
    	int  result = pointMapper.deleteByExample(example);
    	if(result>0)
    	{
    		BaseResponse adminLoginResp = new BaseResponse();
    		adminLoginResp.setStatus("0");
    		adminLoginResp.setResultMsg("删除成功");
    		String json = JSON.toJSONString(adminLoginResp);
    		return json;
    	}else {
    		BaseResponse adminLoginResp = new BaseResponse();
    		adminLoginResp.setStatus("-1");
    		adminLoginResp.setResultMsg("删除失败");
    		String json = JSON.toJSONString(adminLoginResp);
    		return json;
    	}	
    	
    }
    
    @ApiOperation(value="会员积分创建", notes="根据商户后台创建")
    @RequestMapping(value={"/memberPointCreate"}, method=RequestMethod.POST)
    public String memberPointCreate(@RequestBody PointEntity memberEntity) {
    	Example example =new Example(PointEntity.class);
    	example.createCriteria().andEqualTo("member_id",memberEntity.getMember_id());
    	PointEntity  result = pointMapper.selectOneByExample(example);
    	if(result!=null)
    	{
    		BaseResponse adminLoginResp = new BaseResponse();
    		adminLoginResp.setStatus("-1");
    		adminLoginResp.setResultMsg("创建失败,账号已存在！");
    		String json = JSON.toJSONString(adminLoginResp);
    		return json;
    	}else {
    		
    		int res=pointMapper.insertSelective(memberEntity);
    		if(res>0)
    		{
    			BaseResponse adminLoginResp = new BaseResponse();
        		adminLoginResp.setStatus("0");
        		adminLoginResp.setResultMsg("创建成功");
        		String json = JSON.toJSONString(adminLoginResp);
        		return json;
    		}else {
    			BaseResponse adminLoginResp = new BaseResponse();
        		adminLoginResp.setStatus("-1");
        		adminLoginResp.setResultMsg("保存失败");
        		String json = JSON.toJSONString(adminLoginResp);
        		return json;
    		}
    		
    	}	
    	
    }
    
    @ApiOperation(value="会员积分修改", notes="根据ID进行修改")
    @RequestMapping(value={"/memberPointUpdate"}, method=RequestMethod.POST)
    public String memberPointUpdate(@RequestBody PointEntity memberEntity) {
    	Example example =new Example(PointEntity.class);
    	example.createCriteria().andEqualTo("id",memberEntity.getId());
    	PointEntity  result = pointMapper.selectOneByExample(example);
    	if(result==null)
    	{
    		BaseResponse adminLoginResp = new BaseResponse();
    		adminLoginResp.setStatus("-1");
    		adminLoginResp.setResultMsg("账号数据不存在！");
    		String json = JSON.toJSONString(adminLoginResp);
    		return json;
    	}else {
    		
    		int res=pointMapper.updateByExampleSelective(memberEntity, example);
    		if(res>0)
    		{
    			BaseResponse adminLoginResp = new BaseResponse();
        		adminLoginResp.setStatus("0");
        		adminLoginResp.setResultMsg("保存成功");
        		String json = JSON.toJSONString(adminLoginResp);
        		return json;
    		}else {
    			BaseResponse adminLoginResp = new BaseResponse();
        		adminLoginResp.setStatus("-1");
        		adminLoginResp.setResultMsg("保存失败");
        		String json = JSON.toJSONString(adminLoginResp);
        		return json;
    		}
    		
    	}	
    	
    }
    
    
    
    //变换操作
    @ApiOperation(value="会员积分变换", notes="根据ID进行修改")
    @RequestMapping(value={"/memberPointExchange"}, method=RequestMethod.POST)
    public String memberPointExchange(@RequestBody List<PointEntity> memberEntitys
//    		,
//    		@RequestParam(value = "operationType")Integer operationType,
//    		@RequestParam(value = "note")String note
    		) {
    	String json="";
    	try {
    		for(PointEntity memberEntity:memberEntitys )
    		{
    			json =pointService.operation_point_change(memberEntity.getMember_id()+"", memberEntity.getUsedpoits(), memberEntity.getOperationType()+"", memberEntity.getNote());
    			BaseResponse response =JSON.parseObject(json, BaseResponse.class);
    			if(!response.getStatus().equals("0")) 
    			{
    				new Exception("1234");
    			}
    		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("积分变换失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
	
    	return json;
    }
    //兑换操作
    @ApiOperation(value="会员积分兑换", notes="根据什么都没有")
    @RequestMapping(value={"/memberPointRecord"}, method=RequestMethod.POST)
    public String memberPointRecord(@RequestBody List<PointEntity> memberEntitys
//    		,
//    		@RequestParam(value = "operationType")Integer operationType,
//    		@RequestParam(value = "note")String note,
//    		@RequestParam(value = "giftid")String giftid
    		) {
    	String json="";
    	try {
    		for(PointEntity memberEntity:memberEntitys )
    		{
    			json =pointService.operation_point_record(memberEntity.getMember_id()+"", memberEntity.getUsedpoits(),  memberEntity.getOperationType()+"", memberEntity.getNote(),memberEntity.getGiftid());
    			BaseResponse response =JSON.parseObject(json, BaseResponse.class);
    			if(!response.getStatus().equals("0")) 
    			{
    				new Exception("1234");
    			}
    		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("积分兑换失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
	
    	return json;
    }
    
    //兑换操作
    @ApiOperation(value="会员积分清除", notes="根据什么都没有")
    @RequestMapping(value={"/memberPointsClear"}, method=RequestMethod.POST)
    public String memberPointsClear(@RequestBody List<PointEntity> memberEntitys) {
    	String json="";
    	try {
    		for(PointEntity memberEntity:memberEntitys )
    		{
    			json =pointService.operation_point_change_clear(memberEntity.getMember_id()+"", memberEntity.getUsedpoits(),"0", memberEntity.getNote());
    			BaseResponse response =JSON.parseObject(json, BaseResponse.class);
    			if(!response.getStatus().equals("0")) 
    			{
    				new Exception("1234");
    			}
    		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("积分兑换失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
	
    	return json;
    }
    

}
