package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
@ApiModel(value="auth_group", description="权限组实体")
@Table(name = "auth_group")
public class GroupEntity implements Serializable{
	  private String id;
      private String name;
      private String description;
      private String status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "GroupEntity [id=" + id + ", name=" + name + ", description=" + description + ", status=" + status + "]";
	}
      
      
}
