package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "auth_rule", description = "权限组实体")
@Table(name = "auth_rule")
public class GroupRuleEntity implements Serializable {
	private String id;
	private String url;
	@Column(name="groupId")
	private String groupId;
	private String auth;
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GroupRuleEntity [id=" + id + ", url=" + url + ", groupId=" + groupId + ", auth=" + auth + ", status="
				+ status + "]";
	}

}
