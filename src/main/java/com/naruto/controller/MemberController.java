package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.AdminEntity;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.response.UserLoginResp;
import com.naruto.service.MemberService;
import com.naruto.service.PointService;
import com.naruto.tools.StringUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "会员模块")
@RequestMapping("member")
public class MemberController {
	@Autowired
	MemberMapper schoolMapper;

	@Autowired
	MemberService memberService;

	@ApiOperation(value = "会员用户列表", notes = "根据pageNum进行分页查询")
	@RequestMapping(value = { "/memberList" }, method = RequestMethod.POST)
	public String menberlist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

		String json="";
		try {
			json = memberService.getMember(pageNum);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("获取失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;
	}
	
	@ApiOperation(value = "会员用户列表条件查询", notes = "根据会员号进行分页查询")
	@RequestMapping(value = { "/memberListById" }, method = RequestMethod.POST)
	public String memberListById(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(value = "condition") String condition) {
		if(condition == null)
		{
			condition ="";
		}
		String json="";
		try {
			json = memberService.getMemberById(pageNum,condition);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("获取失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;
	}

	@ApiOperation(value = "会员用户信息详情", notes = "根据ID进行查询")
	@RequestMapping(value = { "/memberDetail" }, method = RequestMethod.POST)
	public String menberById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		String json="";
		try {
			json = memberService.getMemberDetail(id);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("获取失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;

		// try {
		// String json =pointService.getSel(id);
		// return json;
		// } catch (Exception e) {
		// // TODO: handle exception
		// return "{resultCode:400}";
		// }

	}

	@ApiOperation(value = "会员用户信息删除", notes = "根据ID进行删除")
	@RequestMapping(value = { "/memberDel" }, method = RequestMethod.POST)
	public String menberdelById(@RequestParam(value = "id") Integer id) {
		Example example = new Example(MemberEntity.class);
		example.createCriteria().andEqualTo("id", id);
		int result = schoolMapper.deleteByExample(example);
		if (result > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("删除成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("删除失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "会员用户信息前后台通用创建", notes = "")
	@RequestMapping(value = { "/memberCreate" }, method = RequestMethod.POST)
	public String menbercreateById(@RequestBody MemberEntity memberEntity) {
		String json="";
		try {
			json = memberService.createMember(memberEntity);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("操作失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;

	}

	@ApiOperation(value = "会员用户信息前后台通用修改", notes = "根据ID进行修改")
	@RequestMapping(value = { "/memberUpdate" }, method = RequestMethod.POST)
	public String menberupdateById(@RequestBody MemberEntity memberEntity) {
		if(memberEntity.getId()==0) 
		{
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-10");
			adminLoginResp.setResultMsg("id不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		Example example = new Example(MemberEntity.class);
		example.createCriteria().andEqualTo("id", memberEntity.getId());
		MemberEntity result = schoolMapper.selectOneByExample(example);
		if (result == null) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("账号数据不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {

			int res = schoolMapper.updateByExampleSelective(memberEntity, example);
			if (res > 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("0");
				adminLoginResp.setResultMsg("保存成功");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("保存失败");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}

		}

	}

	
	
	
}
