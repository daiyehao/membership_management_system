package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "auth_group_access", description = "权限组与用户的关系实体")
@Table(name = "auth_group_access")
public class GroupAccessEntity implements Serializable {
	private String id;
	@Column(name="uid")
	private String uid;
	@Column(name="groupId")
	private String groupId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	@Override
	public String toString() {
		return "GroupRuleEntity [id=" + id + ", uid=" + uid + ", groupId=" + groupId + "]";
	}
	
}
