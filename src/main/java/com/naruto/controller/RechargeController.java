package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.MemberRankMapper;
import com.naruto.dao.RechargeMapper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.RechargeEntity;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.PointEntity;
import com.naruto.entity.RechargeEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.service.MemberService;
import com.naruto.service.PointService;
import com.naruto.service.RechargeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "储值模块")
@RequestMapping("recharge")
public class RechargeController {
	@Autowired
	RechargeMapper schoolMapper;

	@Autowired
	RechargeService rechargeService;
	@Autowired
	MemberService memberService;
	
	@ApiOperation(value = "储值列表", notes = "根据pageNum进行分页查询")
	@RequestMapping(value = { "/rechargeList" }, method = RequestMethod.POST)
	public String rechargelist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

		String json="";
		try {
			json = memberService.getMember(pageNum);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("获取失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;
	}

	@ApiOperation(value = "储值信息详情", notes = "根据ID进行查询")
	@RequestMapping(value = { "/rechargeDetail" }, method = RequestMethod.POST)
	public String rechargeById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		String json="";
		try {
			json = memberService.getMemberDetail(id);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("获取失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;
	}

	@ApiOperation(value = "储值信息删除", notes = "根据ID进行删除")
	@RequestMapping(value = { "/rechargeDel" }, method = RequestMethod.POST)
	public String rechargedelById(@RequestParam(value = "id") Integer id) {
		Example example = new Example(RechargeEntity.class);
		example.createCriteria().andEqualTo("id", id);
		int result = schoolMapper.deleteByExample(example);
		if (result > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("删除成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("删除失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "储值信息后台创建", notes = "根据商户后台创建")
	@RequestMapping(value = { "/rechargeCreate" }, method = RequestMethod.POST)
	public String rechargecreateById(@RequestBody RechargeEntity memberEntity) {

		int res = schoolMapper.insertSelective(memberEntity);
		if (res > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("创建成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("保存失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "储值后台修改", notes = "根据ID进行修改")
	@RequestMapping(value = { "/rechargeUpdate" }, method = RequestMethod.POST)
	public String rechargeUpdateById(@RequestBody RechargeEntity memberEntity) {
		if(memberEntity.getId()==0) 
		{
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-10");
			adminLoginResp.setResultMsg("id不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		Example example = new Example(RechargeEntity.class);
		example.createCriteria().andEqualTo("id", memberEntity.getId());
		RechargeEntity result = schoolMapper.selectOneByExample(example);
		if (result == null) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("账号数据不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {

			int res = schoolMapper.updateByExampleSelective(memberEntity, example);
			if (res > 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("0");
				adminLoginResp.setResultMsg("保存成功");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("保存失败");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}

		}

	}
	
	 //变换操作
    @ApiOperation(value="会员储值变换", notes="根据ID进行修改")
    @RequestMapping(value={"/menberRechargeExchange"}, method=RequestMethod.POST)
    public String menberRechargeExchange(@RequestBody List<RechargeEntity> memberEntitys,
    		@RequestParam(value = "operationType")Integer operationType,
    		@RequestParam(value = "note")String note) {
    	String json="";
    	try {
    		for(RechargeEntity memberEntity:memberEntitys )
    		{
    			json =rechargeService.operation_recharge_change(memberEntity.getMember_id()+"", memberEntity.getRechargeAmount(), operationType+"", note);
    			BaseResponse response =JSON.parseObject(json, BaseResponse.class);
    			if(!response.getStatus().equals("0")) 
    			{
    				new Exception("1234");
    			}
    		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("储值变换失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
	
    	return json;
    }

    //兑换操作
    @ApiOperation(value="会员积分清除", notes="根据什么都没有")
    @RequestMapping(value={"/menberRechargeClear"}, method=RequestMethod.POST)
    public String menberRechargeClear(@RequestBody List<RechargeEntity> memberEntitys,
    		@RequestParam(value = "note")String note) {
    	String json="";
    	try {
    		for(RechargeEntity memberEntity:memberEntitys )
    		{
    			json =rechargeService.operation_point_change_clear(memberEntity.getMember_id()+"", memberEntity.getRechargeAmount(),"0", note);
    			BaseResponse response =JSON.parseObject(json, BaseResponse.class);
    			if(!response.getStatus().equals("0")) 
    			{
    				new Exception("1234");
    			}
    		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("积分兑换失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
	
    	return json;
    }
}
