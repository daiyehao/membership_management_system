package com.naruto.response;

import com.naruto.entity.AdminEntity;
import com.naruto.entity.PointChangeEntity;
import com.naruto.entity.PointEntity;

public class PointsResp extends BaseResponse<PointsResp>{

	private PointEntity point;
	private PointChangeEntity pointchange;

	public PointEntity getPoint() {
		return point;
	}

	public void setPoint(PointEntity point) {
		this.point = point;
	}

	public PointChangeEntity getPointchange() {
		return pointchange;
	}

	public void setPointchange(PointChangeEntity pointchange) {
		this.pointchange = pointchange;
	}

	
	
	
	
	
}
