package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.ActivityMapper;
import com.naruto.dao.GiftMapper;
import com.naruto.dao.ActivityMapper;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.MemberRankMapper;
import com.naruto.dao.PointMapper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.ActivityEntity;
import com.naruto.entity.GiftEntity;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.PointEntity;
import com.naruto.entity.ActivityEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.response.UserLoginResp;
import com.naruto.service.MemberService;
import com.naruto.service.PointService;
import com.naruto.tools.StringUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "前端模块")
@RequestMapping("public")
public class PublicController {
	@Autowired
	ActivityMapper activityMapper;

	@Autowired
	MemberMapper memberMapper;

	@Autowired
	MemberService memberService;
	
	@Autowired
	PointMapper pointMapper;
	
	@Autowired
	PointService pointService;
	
	@Autowired
	GiftMapper giftMapper;
	
	@ApiOperation(value = "活动列表", notes = "根据pageNum进行分页查询")
	@RequestMapping(value = { "/activityList" }, method = RequestMethod.POST)
	public String activitylist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

		PageHelper.startPage(pageNum, 5);
		List<ActivityEntity> blogList = activityMapper.selectAll();
		PageInfo<ActivityEntity> pageInfo = new PageInfo<ActivityEntity>(blogList);

		String json = JSON.toJSONString(pageInfo);
		System.out.println(json);
		return json;
	}

	@ApiOperation(value = "活动信息详情", notes = "根据ID进行查询")
	@RequestMapping(value = { "/activityDetail" }, method = RequestMethod.POST)
	public String activityById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		Example example = new Example(ActivityEntity.class);
		example.createCriteria().andEqualTo("id", id);
		ActivityEntity entity = activityMapper.selectOneByExample(example);
		System.out.println(entity.toString());
		String json = JSON.toJSONString(entity);
		System.out.println(json);
		return json;
	}
	
	@ApiOperation(value = "前端会员登录", notes = "会员")
	@ApiResponses({ @ApiResponse(code = 400, message = "请求参数没填好"),
			@ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对") })
	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public String login(@RequestBody MemberEntity admin) {
		//List<AdminEntity> entitys = schoolMapper.selectAll();
		Example example =new Example(MemberEntity.class);
		example.createCriteria().andEqualTo("username", admin.getUsername()).andEqualTo("password", admin.getPassword());
		MemberEntity entitys = memberMapper.selectOneByExample(example);
		if(entitys!=null)
		{
			String token = StringUtil.GetGUID();
			entitys.setTokenId(token);
			memberMapper.updateByExample(entitys, example);
			UserLoginResp<MemberEntity> adminLoginResp=new UserLoginResp();
			adminLoginResp.setUser(entitys);
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("登录成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}else 
		{
			UserLoginResp adminLoginResp=new UserLoginResp();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("登录失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
	}
	
	@ApiOperation(value = "前端注册会员账号检测存在", notes = "根据username")
	@RequestMapping(value = { "/memberCheck" }, method = RequestMethod.POST)
	public String memberCheck(@RequestBody MemberEntity memberEntity) {
		String json="";
		try {
			json = memberService.checkUser(memberEntity);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("操作失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;

	}

	@ApiOperation(value = "会员用户信息前后台通用创建", notes = "")
	@RequestMapping(value = { "/memberCreate" }, method = RequestMethod.POST)
	public String menbercreateById(@RequestBody MemberEntity memberEntity) {
		String json="";
		try {
			json = memberService.createMember(memberEntity);
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			BaseResponse pointsResp = new BaseResponse();
			pointsResp.setStatus("502");
			pointsResp.setResultMsg("操作失败！");
			json = JSON.toJSONString(pointsResp);
			return json;
		}
		return json;

	}
	
	 @ApiOperation(value="会员积分列表", notes="根据pageNum进行分页查询")
	    @RequestMapping(value={"/memberPointList"}, method=RequestMethod.POST)
		public String memberPointList(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum) {
	    	
	    	 PageHelper.startPage(pageNum,5);
	         List<PointEntity> blogList = pointMapper.selectAll();
	         PageInfo<PointEntity> pageInfo = new PageInfo<PointEntity>(blogList);
	    	
			System.out.println(pageInfo.toString());
			String json =JSON.toJSONString(pageInfo);
			System.out.println(json);
			return json;
		}
	    @ApiOperation(value="会员积分详情", notes="根据ID进行查询")
	    @RequestMapping(value={"/memberPointDetail"}, method=RequestMethod.POST)
	    public String memberPointDetail(@RequestParam(value = "id",defaultValue = "1")Integer id) {
	    	Example example =new Example(PointEntity.class);
	    	example.createCriteria().andEqualTo("id",id);
	    	PointEntity  entity = pointMapper.selectOneByExample(example);
			System.out.println(entity.toString());
			String json =JSON.toJSONString(entity);
			System.out.println(json);
			return json;
		}
	    
	    @ApiOperation(value = "礼品列表", notes = "根据pageNum进行分页查询")
		@RequestMapping(value = { "/giftList" }, method = RequestMethod.POST)
		public String giftlist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

			PageHelper.startPage(pageNum, 5);
			List<GiftEntity> blogList = giftMapper.selectAll();
			PageInfo<GiftEntity> pageInfo = new PageInfo<GiftEntity>(blogList);

			String json = JSON.toJSONString(pageInfo);
			System.out.println(json);
			return json;
		}

		@ApiOperation(value = "礼品信息详情", notes = "根据ID进行查询")
		@RequestMapping(value = { "/giftDetail" }, method = RequestMethod.POST)
		public String giftById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
			Example example = new Example(GiftEntity.class);
			example.createCriteria().andEqualTo("id", id);
			GiftEntity entity = giftMapper.selectOneByExample(example);
			System.out.println(entity.toString());
			String json = JSON.toJSONString(entity);
			System.out.println(json);
			return json;
		}
	    

}
