package com.naruto.response;

import java.util.List;

import com.naruto.entity.AdminEntity;
import com.naruto.mbean.UrlBean;

public class UrlResp extends BaseResponse<UrlResp>{

	private List<UrlBean> urlBeans;

	public List<UrlBean> getUrlBeans() {
		return urlBeans;
	}

	public void setUrlBeans(List<UrlBean> urlBeans) {
		this.urlBeans = urlBeans;
	}

	

	
	
}
