-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2018-04-30 16:16:06
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `academy`
--

-- --------------------------------------------------------

--
-- 表的结构 `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `state` varchar(6) NOT NULL,
  `content` text NOT NULL,
  `activityname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- 转存表中的数据 `activity`
--

INSERT INTO `activity` (`id`, `start`, `end`, `state`, `content`, `activityname`) VALUES
(13, '2017-11-13', '2017-11-30', '可用', '免费获得果汁一杯', '双12'),
(14, '2017-11-06', '2018-12-01', '可用', '免费领取柠檬水一杯', '双11'),
(15, '2018-03-08', '2018-04-28', '可用', '免费领取20积分 ', '五一活动'),
(16, '2018-04-01', '2018-04-28', '可用', '领取果汁1杯', '清明');

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(10) DEFAULT '0' COMMENT '0是商户，1是管理员',
  `tokenId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `type`, `tokenId`) VALUES
(1, 'string', 'string', '1', '692a79e83fa64648a878b3ee1ced50ad');

-- --------------------------------------------------------

--
-- 表的结构 `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '组名称',
  `description` varchar(50) NOT NULL COMMENT '组描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '组状态：为1正常，为0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='权限组' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`, `description`, `status`) VALUES
(1, 'name', 'desc', 1);

-- --------------------------------------------------------

--
-- 表的结构 `auth_group_access`
--

CREATE TABLE IF NOT EXISTS `auth_group_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL,
  `groupId` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户和组的对应关系' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `auth_group_access`
--

INSERT INTO `auth_group_access` (`id`, `uid`, `groupId`) VALUES
(1, 1, '1'),
(2, 1, '1');

-- --------------------------------------------------------

--
-- 表的结构 `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `url` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `groupId` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '权限所属组的ID',
  `auth` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '权限数值',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='权限细节' AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `auth_rule`
--

INSERT INTO `auth_rule` (`id`, `url`, `groupId`, `auth`, `status`) VALUES
(1, 'menu/list', 1, 1, 1),
(10, 'string', 1, 0, 1),
(11, 'string', 1, 0, 1),
(12, 'string', 2, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `gifts`
--

CREATE TABLE IF NOT EXISTS `gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `giftsName` varchar(50) NOT NULL,
  `giftstype` varchar(20) NOT NULL,
  `giftscount` int(11) NOT NULL,
  `exchangepoints` decimal(10,0) NOT NULL,
  `note` varchar(100) DEFAULT NULL,
  `createtie` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `sex` varchar(55) DEFAULT NULL,
  `school` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `performance` varchar(8) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `registerTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `weixinno` varchar(30) DEFAULT NULL,
  `weixinimg` varchar(200) DEFAULT NULL,
  `rank_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- 转存表中的数据 `member`
--

INSERT INTO `member` (`id`, `name`, `telephone`, `sex`, `school`, `birthday`, `email`, `major`, `number`, `address`, `performance`, `password`, `username`, `openid`, `registerTime`, `updateTime`, `weixinno`, `weixinimg`, `rank_id`) VALUES
(1, 'cs', '13322224444', '男', '湖南财政经济学院', '1996-01-06', '123@qq.com', '计算机科学与技术', NULL, NULL, '100', 'e10adc3949ba59abbe56e057f20f883e', 'cs', NULL, '2018-04-20 00:54:32', '2018-04-30 06:22:35', NULL, NULL, 1),
(2, 'yy', '123', '男', '456', '2017-10-04', '4564', '数学', '4564', '123', '0', NULL, 'qwe', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(3, 'ww', '456', '男', 'fsdfsdf', '2017-10-10', 'sfsdf', 'dasda', 'fssdf', '000', '0', NULL, 'q', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(4, 'ww', '456', '男', 'fsdfsdf', '2017-10-09', '123', '45', '5', '545', '0', NULL, 'wwq', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(5, 'eed', 'dd', 'dd', 'dd', '2017-10-03', 'dd', 'dd', 'dd', 'dd', '0', NULL, 'ggf', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(6, 'qeq', '56456', '45', '456', '2017-10-02', '', '', '', '', '0', NULL, '122', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(7, '454', '15084978667', 'female', '456', NULL, NULL, NULL, NULL, NULL, '0', '123456', '96554', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(8, '852', '45456', '女', '5454', '2017-11-15', '328863397@qq.com', '4546', NULL, NULL, '80', 'e10adc3949ba59abbe56e057f20f883e', '456', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(9, '小明', '15084978667', 'male', '湖南大学', NULL, NULL, NULL, NULL, NULL, '0', 'e10adc3949ba59abbe56e057f20f883e', '大狂魔', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(10, 'cyh', '1544', 'male', '45456', NULL, NULL, NULL, NULL, NULL, '0', 'e10adc3949ba59abbe56e057f20f883e', 'cyh', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(11, '4545', '8787', 'male', '8787878', NULL, NULL, NULL, NULL, NULL, '0', 'e10adc3949ba59abbe56e057f20f883e', '4545', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(12, 'cyh', '15084978667', '男', '湖南大学', '2017-11-15', '328863397@qq.com', '美术', '201508010520', '湖南', '0', 'e10adc3949ba59abbe56e057f20f883e', 'cyh222', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(13, '111', '15084978665', '男', '湖南大学', NULL, NULL, NULL, NULL, NULL, '0', 'e10adc3949ba59abbe56e057f20f883e', 'cyh111', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(17, '谢利思', '15200820259', '女', NULL, NULL, NULL, NULL, NULL, NULL, '60', 'e10adc3949ba59abbe56e057f20f883e', 'xls', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(18, '朱望', '15200820258', '男', NULL, NULL, NULL, NULL, NULL, NULL, '60', 'e10adc3949ba59abbe56e057f20f883e', 'zw', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(19, '测试', '15200820259', '女', NULL, NULL, NULL, NULL, NULL, NULL, '60', '5f3db235dd94c7456644d6ab8fab1a5e', 'test', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(20, '123', '15200820259', '男', NULL, NULL, NULL, NULL, NULL, NULL, '60', '25d55ad283aa400af464c76d713c07ad', '123', NULL, '2018-04-20 00:54:32', '2018-04-19 16:57:33', NULL, NULL, 0),
(21, '11', '15200820259', '男', NULL, NULL, NULL, NULL, NULL, NULL, '60', 'e10adc3949ba59abbe56e057f20f883e', '11', NULL, '2018-04-20 00:54:32', '2018-04-30 06:20:37', NULL, NULL, 1),
(23, 'bbb', '15200820259', '男', NULL, NULL, NULL, NULL, NULL, NULL, '60', '875f26fdb1cecf20ceb4ca028263dec6', 'bbbbbb', NULL, '2018-04-20 00:54:32', '2018-04-30 06:20:33', NULL, NULL, 1),
(24, '1', '1', '1', '1', '2018-04-03', '22', '22', '22', '22222222', '33', '342432', '2342', '33333333', '2018-04-30 14:52:41', '2018-04-30 06:52:41', '3', '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `points`
--

CREATE TABLE IF NOT EXISTS `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(20) NOT NULL,
  `cardNo` varchar(20) NOT NULL,
  `totalpoits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `usedpoits` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='会员积分表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `points`
--

INSERT INTO `points` (`id`, `member_id`, `cardNo`, `totalpoits`, `usedpoits`) VALUES
(1, 'string', 'string', '0.00', '0.00'),
(2, '1', 'string', '30.00', '0.00'),
(3, '3', 'string', '10.00', '0.00'),
(4, '2', '1', '0.00', '0.00');

-- --------------------------------------------------------

--
-- 表的结构 `pointsexchange`
--

CREATE TABLE IF NOT EXISTS `pointsexchange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(20) NOT NULL,
  `card_id` varchar(20) NOT NULL,
  `operationType` varchar(50) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1.积分增加 0积分减少',
  `points` decimal(10,2) NOT NULL,
  `note` varchar(100) DEFAULT NULL,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pointsexchange`
--

INSERT INTO `pointsexchange` (`id`, `member_id`, `card_id`, `operationType`, `type`, `points`, `note`, `createtime`) VALUES
(1, '1', '1', '1', 0, '100.00', 'add', NULL),
(2, '1', '1', '1', 0, '10.00', '10', '2018-04-30 16:51:59'),
(3, '1', '1', '1', 0, '10.00', '10', '2018-04-30 16:52:55');

-- --------------------------------------------------------

--
-- 表的结构 `pointsrecord`
--

CREATE TABLE IF NOT EXISTS `pointsrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(20) NOT NULL,
  `cardNo` varchar(20) NOT NULL,
  `operationType` varchar(100) NOT NULL,
  `giftid` int(11) NOT NULL,
  `giftNumber` int(11) NOT NULL,
  `Note` varchar(100) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rankName` varchar(50) NOT NULL,
  `rankcount` decimal(10,2) NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `note` varchar(100) DEFAULT NULL,
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `rank`
--

INSERT INTO `rank` (`id`, `rankName`, `rankcount`, `starttime`, `endtime`, `note`, `createtime`) VALUES
(1, '普通会员', '100.00', '2018-04-30 15:47:36', '2018-04-30 15:47:41', NULL, '2018-04-30 14:20:06');

-- --------------------------------------------------------

--
-- 表的结构 `recharge`
--

CREATE TABLE IF NOT EXISTS `recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(20) NOT NULL,
  `cardNo` varchar(20) DEFAULT NULL,
  `rechargeAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='会员储值表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `recharge`
--

INSERT INTO `recharge` (`id`, `member_id`, `cardNo`, `rechargeAmount`) VALUES
(1, 'string', 'string', '0.00'),
(2, '1', 'string', '30.00'),
(3, '3', 'string', '10.00'),
(4, '2', '1', '0.00');

-- --------------------------------------------------------

--
-- 表的结构 `rechargerecord`
--

CREATE TABLE IF NOT EXISTS `rechargerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `rechargeAmount` decimal(10,2) DEFAULT '0.00' COMMENT '充值/消费金额',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `operationtype` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `memo` varchar(400) DEFAULT NULL,
  `prop1` varchar(50) DEFAULT NULL,
  `prop2` varchar(50) DEFAULT NULL,
  `adduser` varchar(50) NOT NULL,
  `addtime` datetime NOT NULL,
  `modifyuser` varchar(50) DEFAULT NULL,
  `modifytime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `school`
--

INSERT INTO `school` (`id`, `name`, `code`, `memo`, `prop1`, `prop2`, `adduser`, `addtime`, `modifyuser`, `modifytime`) VALUES
(1, '长沙第1中学', NULL, NULL, NULL, NULL, 'admin', '2018-01-27 16:58:06', NULL, NULL),
(2, '长沙第2中学', NULL, NULL, NULL, NULL, 'admin', '2018-01-27 16:58:06', NULL, NULL),
(3, '长沙第3中学', NULL, NULL, NULL, NULL, 'admin', '2018-01-27 16:58:06', NULL, NULL),
(4, 'xxx大学', NULL, 'memo4', NULL, NULL, 'system', '2018-04-06 15:03:46', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
