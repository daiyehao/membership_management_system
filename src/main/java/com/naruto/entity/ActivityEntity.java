package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "activity")
public class ActivityEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	private String start;
	private String end;
	private String state;
	private String content;
	private String activityname;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getActivityname() {
		return activityname;
	}
	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}
	@Override
	public String toString() {
		return "ActivityEntity [id=" + id + ", start=" + start + ", end=" + end + ", state=" + state + ", content="
				+ content + ", activityname=" + activityname + "]";
	}
	
	
}
