package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tk.mybatis.mapper.annotation.ColumnType;
@ApiModel(value="admin", description="admin实体")

@Table(name = "admin")
public class AdminEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
    @ApiModelProperty("描述一个model的属性")
	private String type;
	private String password;
	private String username;
	@Column(name="tokenId")
	private String tokenId;
	
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public AdminEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AdminEntity(Integer id, String type, String password, String username, String tokenId) {
		super();
		this.id = id;
		this.type = type;
		this.password = password;
		this.username = username;
		this.tokenId = tokenId;
	}
	@Override
	public String toString() {
		return "AdminEntity [id=" + id + ", type=" + type + ", password=" + password + ", username=" + username
				+ ", tokenId=" + tokenId + "]";
	}
	
	
	
}
