package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.naruto.Interceptor.annotation.Auth;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.School;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
public class SampleController {
	@Autowired
	SchoolMapper schoolMapper;
	@Auth(user="admin")
    @ApiOperation(value="创建用户", notes="根据User对象创建用户")
    @ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User")
    @RequestMapping(value={"/hello"}, method=RequestMethod.GET)
	public String hello() {
		List<School> entitys =schoolMapper.getAll();
		System.out.println(entitys.toString());
		String json =JSON.toJSONString(entitys);
		System.out.println(json);
		return json;
	}

}
