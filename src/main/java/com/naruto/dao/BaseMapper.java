package com.naruto.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

@Mapper
public interface BaseMapper {

	public static final String SQLFIELD = "sqlField";

	@SelectProvider(type = BaseSqlProvider.class, method = "getSQL")
	public List<Map<String, Object>> fetch(Map<String, Object> paramMap);

	@SelectProvider(type = BaseSqlProvider.class, method = "getSQL")
	public Page<Map<String, Object>> fetchPage(Map<String, Object> paramMap);

	@SelectProvider(type = BaseSqlProvider.class, method = "getSQL")
	public String executeString(Map<String, Object> paramMap);

	@UpdateProvider(type = BaseSqlProvider.class, method = "getSQL")
	public int executeUpdate(Map<String, Object> paramMap);
	@SelectProvider(type = BaseSqlProvider.class, method = "getSQL")
	public long executeLong(Map<String, Object> paramMap);

	class BaseSqlProvider {
		public String getSQL(Map<String, Object> paramMap) {
			return paramMap.get(SQLFIELD).toString();
		}
	}

}
