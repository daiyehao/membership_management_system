package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.GiftMapper;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.MemberRankMapper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.GiftEntity;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.GiftEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.service.PointService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "礼品模块")
@RequestMapping("gift")
public class GiftController {
	@Autowired
	GiftMapper schoolMapper;

	@ApiOperation(value = "礼品列表", notes = "根据pageNum进行分页查询")
	@RequestMapping(value = { "/giftList" }, method = RequestMethod.POST)
	public String giftlist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

		PageHelper.startPage(pageNum, 5);
		List<GiftEntity> blogList = schoolMapper.selectAll();
		PageInfo<GiftEntity> pageInfo = new PageInfo<GiftEntity>(blogList);

		String json = JSON.toJSONString(pageInfo);
		System.out.println(json);
		return json;
	}

	@ApiOperation(value = "礼品信息详情", notes = "根据ID进行查询")
	@RequestMapping(value = { "/giftDetail" }, method = RequestMethod.POST)
	public String giftById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		Example example = new Example(GiftEntity.class);
		example.createCriteria().andEqualTo("id", id);
		GiftEntity entity = schoolMapper.selectOneByExample(example);
		System.out.println(entity.toString());
		String json = JSON.toJSONString(entity);
		System.out.println(json);
		return json;
	}

	@ApiOperation(value = "礼品信息删除", notes = "根据ID进行删除")
	@RequestMapping(value = { "/giftDel" }, method = RequestMethod.POST)
	public String giftdelById(@RequestParam(value = "id") Integer id) {
		Example example = new Example(GiftEntity.class);
		example.createCriteria().andEqualTo("id", id);
		int result = schoolMapper.deleteByExample(example);
		if (result > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("删除成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("删除失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "礼品信息后台创建", notes = "根据商户后台创建")
	@RequestMapping(value = { "/giftCreate" }, method = RequestMethod.POST)
	public String giftcreateById(@RequestBody GiftEntity memberEntity) {

		int res = schoolMapper.insertSelective(memberEntity);
		if (res > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("创建成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("保存失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "礼品后台修改", notes = "根据ID进行修改")
	@RequestMapping(value = { "/giftUpdate" }, method = RequestMethod.POST)
	public String giftUpdateById(@RequestBody GiftEntity memberEntity) {
		if(memberEntity.getId()==0) 
		{
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-10");
			adminLoginResp.setResultMsg("id不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		Example example = new Example(GiftEntity.class);
		example.createCriteria().andEqualTo("id", memberEntity.getId());
		GiftEntity result = schoolMapper.selectOneByExample(example);
		if (result == null) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("账号数据不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {

			int res = schoolMapper.updateByExampleSelective(memberEntity, example);
			if (res > 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("0");
				adminLoginResp.setResultMsg("保存成功");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("保存失败");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}

		}

	}

}
