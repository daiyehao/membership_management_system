package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "rank")
public class MemberRankEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	@Column(name="rankName")
	private String rankName;
	private String rankcount;
	private String starttime;
	private String endtime;
	private String note;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRankName() {
		return rankName;
	}
	public void setRankName(String rankName) {
		this.rankName = rankName;
	}
	public String getRankcount() {
		return rankcount;
	}
	public void setRankcount(String rankcount) {
		this.rankcount = rankcount;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public MemberRankEntity(Integer id, String rankName, String rankcount, String starttime, String endtime,
			String note) {
		super();
		this.id = id;
		this.rankName = rankName;
		this.rankcount = rankcount;
		this.starttime = starttime;
		this.endtime = endtime;
		this.note = note;
	}
	public MemberRankEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "MemberRankEntity [id=" + id + ", rankName=" + rankName + ", rankcount=" + rankcount + ", starttime="
				+ starttime + ", endtime=" + endtime + ", note=" + note + "]";
	}
	
}
