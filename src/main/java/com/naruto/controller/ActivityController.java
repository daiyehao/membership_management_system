package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.ActivityMapper;
import com.naruto.dao.ActivityMapper;
import com.naruto.dao.MemberMapper;
import com.naruto.dao.MemberRankMapper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.ActivityEntity;
import com.naruto.entity.MemberEntity;
import com.naruto.entity.ActivityEntity;
import com.naruto.entity.School;
import com.naruto.response.BaseResponse;
import com.naruto.service.PointService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "活动模块")
@RequestMapping("activity")
public class ActivityController {
	@Autowired
	ActivityMapper schoolMapper;

	@ApiOperation(value = "活动列表", notes = "根据pageNum进行分页查询")
	@RequestMapping(value = { "/activityList" }, method = RequestMethod.POST)
	public String activitylist(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {

		PageHelper.startPage(pageNum, 5);
		List<ActivityEntity> blogList = schoolMapper.selectAll();
		PageInfo<ActivityEntity> pageInfo = new PageInfo<ActivityEntity>(blogList);

		String json = JSON.toJSONString(pageInfo);
		System.out.println(json);
		return json;
	}

	@ApiOperation(value = "活动信息详情", notes = "根据ID进行查询")
	@RequestMapping(value = { "/activityDetail" }, method = RequestMethod.POST)
	public String activityById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		Example example = new Example(ActivityEntity.class);
		example.createCriteria().andEqualTo("id", id);
		ActivityEntity entity = schoolMapper.selectOneByExample(example);
		System.out.println(entity.toString());
		String json = JSON.toJSONString(entity);
		System.out.println(json);
		return json;
	}

	@ApiOperation(value = "活动信息删除", notes = "根据ID进行删除")
	@RequestMapping(value = { "/activityDel" }, method = RequestMethod.POST)
	public String activitydelById(@RequestParam(value = "id") Integer id) {
		Example example = new Example(ActivityEntity.class);
		example.createCriteria().andEqualTo("id", id);
		int result = schoolMapper.deleteByExample(example);
		if (result > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("删除成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("删除失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "活动信息后台创建", notes = "根据商户后台创建")
	@RequestMapping(value = { "/activityCreate" }, method = RequestMethod.POST)
	public String activitycreateById(@RequestBody ActivityEntity memberEntity) {

		int res = schoolMapper.insertSelective(memberEntity);
		if (res > 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("创建成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("保存失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	@ApiOperation(value = "活动后台修改", notes = "根据ID进行修改")
	@RequestMapping(value = { "/activityUpdate" }, method = RequestMethod.POST)
	public String activityUpdateById(@RequestBody ActivityEntity memberEntity) {
		if(memberEntity.getId()==0) 
		{
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-10");
			adminLoginResp.setResultMsg("id不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		Example example = new Example(ActivityEntity.class);
		example.createCriteria().andEqualTo("id", memberEntity.getId());
		ActivityEntity result = schoolMapper.selectOneByExample(example);
		if (result == null) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("账号数据不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		} else {

			int res = schoolMapper.updateByExampleSelective(memberEntity, example);
			if (res > 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("0");
				adminLoginResp.setResultMsg("保存成功");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("保存失败");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}

		}

	}

}
