package com.naruto.response;

import com.naruto.entity.AdminEntity;

public class UserLoginResp<T> extends BaseResponse<T>{

	private T user;

	public T getUser() {
		return user;
	}

	public void setUser(T user) {
		this.user = user;
	}	
	
}
