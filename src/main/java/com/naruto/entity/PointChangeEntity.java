package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "pointsexchange")
public class PointChangeEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	private String member_id;
	private String card_id;
	private String operation;
	private String type;
	private String points;
	private String note;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public PointChangeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getCard_id() {
		return card_id;
	}
	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Override
	public String toString() {
		return "PointChangeEntity [id=" + id + ", member_id=" + member_id + ", card_id=" + card_id + ", operation="
				+ operation + ", type=" + type + ", points=" + points + ", note=" + note + "]";
	}
	
	
	
}
