package com.naruto.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.naruto.dao.BaseMapper;
import com.naruto.dao.MemberMapper;
import com.naruto.response.BaseResponse;
import com.naruto.response.PointsResp;

@Service
public class PointService {

	@Autowired
	MemberMapper schoolMapper;
	@Autowired
	BaseMapper basicMapper;

	@Transactional
	public String getSel(int id) {
		String sql = "select g.* from auth_groups g, auth_group_access a where g.id= a.groupId";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		JSONArray arr = new JSONArray();
		for (Map<String, Object> map : list) {
			arr.add(map);
		}
		return arr.toJSONString();
	}

	// 积分变换 需要操作两种表 个人积分表、积分变换表里加记录

	@Transactional
	public String operation_point_change(String id, double points, String operationType, String note) throws Exception {
		// select
		String sql_select = "select * from points where member_id= #{id}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql_select);
		paramMap.put("id", id);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		double tpoints = 0;
		double upoints = 0;
		// 取出查询的积分
		if (list != null && list.size() > 0) {
			System.out.println(list.get(0).get("totalpoits"));
			tpoints = Double.parseDouble(list.get(0).get("totalpoits")+"");
			upoints = Double.parseDouble(list.get(0).get("usedpoits")+"");
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		if (operationType.equals("1")) {
			tpoints += points;
		} else {
			tpoints -= points;
			if (tpoints < 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("积分操作失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		}

		// update
		String sql_update = "UPDATE `points` SET totalpoits=#{tatal} ,usedpoits =#{used} WHERE `member_id` = #{id};";
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put(BaseMapper.SQLFIELD, sql_update);
		paramMap2.put("tatal", tpoints);
		paramMap2.put("used", upoints);
		paramMap2.put("id", id);
		int lines = basicMapper.executeUpdate(paramMap2);
		if (lines > 0) {
			// INSERT INTO `academy`.`pointsexchange` (`id`, `member_id`, `card_id`,
			// `operationType`, `type`, `points`, `note`, `createtime`) VALUES (NULL, '1',
			// '1', '1', '', '100', 'add', NULL);
			String sql_add = "INSERT INTO " + "`academy`.`pointsexchange` "
					+ "(`member_id`, `card_id`, `operationType`, `type`, `points`, `note`)" + " VALUES"
					+ " (#{member_id}, #{card_id}, #{operationType}, #{type}, #{points},#{note});";
			Map<String, Object> paramMap3 = new HashMap<String, Object>();
			paramMap3.put(BaseMapper.SQLFIELD, sql_add);
			paramMap3.put("member_id", id);
			paramMap3.put("card_id", id);
			paramMap3.put("operationType", operationType);
			paramMap3.put("type", 0);
			paramMap3.put("points", points);
			paramMap3.put("note", note);
			int lines2 = basicMapper.executeUpdate(paramMap3);
			if (lines2 > 0) {
				PointsResp pointsResp = new PointsResp();
				pointsResp.setStatus("0");
				pointsResp.setResultMsg("积分操作成功！");
				String json = JSON.toJSONString(pointsResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("积分记录添加失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分变换失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

	// 积分兑换 需要操作三张表 个人积分表总积分和使用积分查询、更新；积分兑换表里加记录；礼品表的库存数减少

	@Transactional
	public String operation_point_record(String id, double points, String operationType, String note,String giftid) throws Exception {
		// select
		String sql_select = "select * from points where member_id= #{id}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql_select);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		double tpoints = 0;
		double upoints = 0;
		// 取出查询的积分
		if (list != null && list.size() > 0) {
			tpoints = Double.parseDouble(list.get(0).get("totalpoits")+"");
			upoints = Double.parseDouble(list.get(0).get("usedpoits")+"");
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

		tpoints -= points;
		if (tpoints < 0) {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分操作失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		upoints += points;
		// update
		String sql_update = "UPDATE `academy`.`points` SET totalpoits=#{tatal} ,usedpoits =#{used} WHERE `member_id` = #{id};";
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put(BaseMapper.SQLFIELD, sql_update);
		paramMap2.put("tatal", tpoints);
		paramMap2.put("used", upoints);
		paramMap2.put("id", id);
		int lines = basicMapper.executeUpdate(paramMap2);
		if (lines > 0) {
			String sql_add = "INSERT INTO " + "`academy`.`pointsrecord` "
					+ "(`member_id`, `cardNo`, `operationType`, `giftid`, `note`)" + " VALUES"
					+ " (#{member_id}, #{card_id}, #{operationType}, #{giftid},#{note});";
			Map<String, Object> paramMap3 = new HashMap<String, Object>();
			paramMap3.put(BaseMapper.SQLFIELD, sql_add);
			paramMap3.put("member_id", id);
			paramMap3.put("card_id", id);
			paramMap3.put("operationType", operationType);
			paramMap3.put("giftid", giftid);
			paramMap3.put("note", note);
			int lines2 = basicMapper.executeUpdate(paramMap3);
			if (lines2 > 0) {
				
				String sql_kucun ="UPDATE gifts SET giftscount=gitscout-1 WHERE `id` = '#{id}';";
				Map<String, Object> paramMap4 = new HashMap<String, Object>();
				paramMap4.put(BaseMapper.SQLFIELD, sql_add);
				paramMap4.put("id", giftid);
				int lines4 = basicMapper.executeUpdate(paramMap4);
				if(lines4>0) 
				{
					PointsResp pointsResp = new PointsResp();
					pointsResp.setStatus("0");
					pointsResp.setResultMsg("积分操作成功！");
					String json = JSON.toJSONString(pointsResp);
					return json;
				}else {
					PointsResp pointsResp = new PointsResp();
					pointsResp.setStatus("-1");
					pointsResp.setResultMsg("库存修改成功！");
					String json = JSON.toJSONString(pointsResp);
					return json;
				}
				
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("积分记录添加失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分兑换失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
	}

	// 活动 是限时对某个礼品进行打折兑换 我滴妈耶~ 商城打折有木有
	@Transactional
	public String operation_point_change_clear(String id, double points, String operationType, String note) throws Exception {
		// select
		String sql_select = "select * from points where member_id= #{id}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql_select);
		paramMap.put("id", id);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		double tpoints = 0;
		double upoints = 0;
		// 取出查询的积分
		if (list != null && list.size() > 0) {
			System.out.println(list.get(0).get("totalpoits"));
			tpoints = Double.parseDouble(list.get(0).get("totalpoits")+"");
			upoints = Double.parseDouble(list.get(0).get("usedpoits")+"");
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		tpoints =0;

		// update
		String sql_update = "UPDATE `points` SET totalpoits=#{tatal} ,usedpoits =#{used} WHERE `member_id` = #{id};";
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put(BaseMapper.SQLFIELD, sql_update);
		paramMap2.put("tatal", tpoints);
		paramMap2.put("used", upoints);
		paramMap2.put("id", id);
		int lines = basicMapper.executeUpdate(paramMap2);
		if (lines > 0) {
			// INSERT INTO `academy`.`pointsexchange` (`id`, `member_id`, `card_id`,
			// `operationType`, `type`, `points`, `note`, `createtime`) VALUES (NULL, '1',
			// '1', '1', '', '100', 'add', NULL);
			String sql_add = "INSERT INTO " + "`academy`.`pointsexchange` "
					+ "(`member_id`, `card_id`, `operationType`, `type`, `points`, `note`)" + " VALUES"
					+ " (#{member_id}, #{card_id}, #{operationType}, #{type}, #{points},#{note});";
			Map<String, Object> paramMap3 = new HashMap<String, Object>();
			paramMap3.put(BaseMapper.SQLFIELD, sql_add);
			paramMap3.put("member_id", id);
			paramMap3.put("card_id", id);
			paramMap3.put("operationType", operationType);
			paramMap3.put("type", 0);
			paramMap3.put("points", points);
			paramMap3.put("note", note);
			int lines2 = basicMapper.executeUpdate(paramMap3);
			if (lines2 > 0) {
				PointsResp pointsResp = new PointsResp();
				pointsResp.setStatus("0");
				pointsResp.setResultMsg("积分操作成功！");
				String json = JSON.toJSONString(pointsResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("积分记录添加失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("积分清除失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}

}
