package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "points")
public class PointEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	private String member_id;
	@Column(name="cardNo")
	private String cardNo;
	
	private String operationType;
	private String note;
	private String giftid;

	
	private Double totalpoits=0.00;
	private Double usedpoits=0.00;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public PointEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public Double getTotalpoits() {
		return totalpoits;
	}
	public void setTotalpoits(Double totalpoits) {
		this.totalpoits = totalpoits;
	}
	public Double getUsedpoits() {
		return usedpoits;
	}
	public void setUsedpoits(Double usedpoits) {
		this.usedpoits = usedpoits;
	}
	
	
	
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	public String getGiftid() {
		return giftid;
	}
	public void setGiftid(String giftid) {
		this.giftid = giftid;
	}
	@Override
	public String toString() {
		return "PointEntity [id=" + id + ", member_id=" + member_id + ", cardNo=" + cardNo + ", operationType="
				+ operationType + ", note=" + note + ", totalpoits=" + totalpoits + ", usedpoits=" + usedpoits + "]";
	}

	
	
	
	
}
