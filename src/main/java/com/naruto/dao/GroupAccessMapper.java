package com.naruto.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.jdbc.SQL;

import com.naruto.entity.AdminEntity;
import com.naruto.entity.GroupAccessEntity;
import com.naruto.entity.School;


@Mapper
public interface GroupAccessMapper  extends tk.mybatis.mapper.common.Mapper<GroupAccessEntity>{

//	@Select("select * from _Admin")
//	public List<AdminEntity> getAll();
//
//	@Insert("insert into _Admin (username) values ( #{username}) ")
//	public int add(AdminEntity school);
//
//	@Delete("delete from _Admin where id=#{id}")
//	public int delete(long id);
//
//	@Update("update _Admin set name=#{name} where id=#{id}")
//	public int update(AdminEntity school);
//
//	@Select("select s.id,s.name,t.id as tID,t.name as tname, s.addTime from school s,teacher t where s.id=t.schoolID and s.id=#{schoolID} ")
//	public List<Map<String, Object>> findSchoolTeacher(long schoolID);
//
//	// 动态sql查询
//
//	@SelectProvider(type = SchoolSqlProvider.class, method = "executeString")
//	public String executeString(int id);
//
//	// 方便测试用内部类
//	class SchoolSqlProvider {
//		public String executeString() {
//			SQL s = new SQL();
//			s.SELECT("name").FROM("_Admin").WHERE("id=#{id}");
//			String sql = s.toString();
//			return sql;
//		}
//	}

}
