package com.naruto.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageHelper;
import com.naruto.dao.SchoolMapper;
import com.naruto.entity.School;

/**
 * @author hhp
 * @mail 1228929031@qq.com
 * @date 2018年4月6日
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class SchoolMapperTest {

	@Autowired
	private SchoolMapper schoolMapper;

	@Test
	public void testQuery() {
		List<School> all = schoolMapper.getAll();
		System.out.println(all.toString());
	}

	@Test
	public void testInsert() {
		for (int i = 4; i < 6; i++) {
			School school = new School(i, "学校" + i, null, "memo" + i, null, null, "system", new Date(), null, null);
			int res = schoolMapper.add(school);
			System.out.println("结果：" + res);
		}
	}

	@Test
	public void testDelete() {
		schoolMapper.delete(5);
	}

	@Test
	public void testUpdate() {
		School School = new School();
		School.setId(4);
		School.setName("xxx大学");
		schoolMapper.update(School);
	}

	@Test
	public void testfind() {
		List<Map<String, Object>> find = schoolMapper.findSchoolTeacher(1);
		for (Map<String, Object> map : find) {
			System.out.println(map.get("id") + ":" + map.get("name") + ":" + map.get("addTime") + " tid:" + map.get("tID") + "tname:"
					+ map.get("tname"));

		}
	}

	@Test
	public void testExecuteString() {
		String name = schoolMapper.executeString(2);
		System.out.println(name);

	}

	@Test
	public void testPageExecute() {
		PageHelper.startPage(2, 2);
		List<School> pageResult = schoolMapper.getAll();
		System.out.println(pageResult);
	}

}
