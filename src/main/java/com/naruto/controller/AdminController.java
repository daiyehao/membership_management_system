package com.naruto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.naruto.dao.AdminMapper;
import com.naruto.entity.AdminEntity;
import com.naruto.entity.GroupAccessEntity;
import com.naruto.response.BaseResponse;
import com.naruto.response.UserLoginResp;
import com.naruto.tools.StringUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tk.mybatis.mapper.entity.Example;

@RestController
@Api(value = "管理员模块")
@RequestMapping("admin")
public class AdminController {
	@Autowired
	AdminMapper schoolMapper;

	
	// @ApiImplicitParams({
	// @ApiImplicitParam(name = "type", value = "后台角色", required = true, paramType =
	// "form", dataType = "String"),
	// @ApiImplicitParam(name = "password", value = "后台密码", required = true,
	// paramType = "form", dataType = "String"),
	// @ApiImplicitParam(name = "username", value = "后台账号", required = true,
	// paramType = "form", dataType = "String"),
	// @ApiImplicitParam(name = "tokenId", value = "访问令牌", required = true,
	// paramType = "form", dataType = "String") })

	@ApiOperation(value = "后台登录", notes = "系统只有一个管理员角色，存在多个商户角色")
	@ApiResponses({ @ApiResponse(code = 400, message = "请求参数没填好"),
			@ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对") })
	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public String login(@RequestBody AdminEntity admin) {
		//List<AdminEntity> entitys = schoolMapper.selectAll();
		Example example =new Example(AdminEntity.class);
		example.createCriteria().andEqualTo("username", admin.getUsername()).andEqualTo("password", admin.getPassword());
		AdminEntity entitys = schoolMapper.selectOneByExample(example);
		System.out.println(entitys.toString());
		if(entitys!=null)
		{
			String token = StringUtil.GetGUID();
			entitys.setTokenId(token);
			schoolMapper.updateByExample(entitys, example);
			UserLoginResp adminLoginResp=new UserLoginResp();
			adminLoginResp.setUser(entitys);
			adminLoginResp.setStatus("0");
			adminLoginResp.setResultMsg("登录成功");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}else 
		{
			UserLoginResp adminLoginResp=new UserLoginResp();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("登录失败");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
	}
	
	// 2、创建商户账号
		@SuppressWarnings("unused")
		@ApiOperation(value = "创建商户用户", notes = "")
		@ApiResponses({ @ApiResponse(code = 400, message = "请求参数没填好"),
				@ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对") })
		@RequestMapping(value = { "/createAdmin" }, method = RequestMethod.POST)
		public String bingdUser(@RequestBody AdminEntity adminEntity) {
			//查一下是否有账号了
			Example example = new Example(AdminEntity.class);
			example.createCriteria().andEqualTo("username", adminEntity.getUsername());
			AdminEntity entitys = schoolMapper.selectOneByExample(example);
			System.out.println(entitys.toString());
			BaseResponse adminLoginResp = new BaseResponse();
			if(entitys != null) 
			{
				
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("账号已存在！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}else {
				adminEntity.setType("0");
				schoolMapper.insertSelective(adminEntity);
				adminLoginResp.setStatus("0");
				adminLoginResp.setResultMsg("创建成功！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
			
			
			
		}
	
	
}
