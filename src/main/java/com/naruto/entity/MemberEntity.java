package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "member")
public class MemberEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	private String openid;
	private String name;
	private String username;
	private String weixinno;
	private String weixinimg;
	private String telephone;
	private String email;
	private String sex;
	private String birthday;
	private String password;
	private String school;
	private String major;
	private String address;
	private String performance;
	@Column(name="tokenId")
	private String tokenId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getWeixinno() {
		return weixinno;
	}
	public void setWeixinno(String weixinno) {
		this.weixinno = weixinno;
	}
	public String getWeixinimg() {
		return weixinimg;
	}
	public void setWeixinimg(String weixinimg) {
		this.weixinimg = weixinimg;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	
	
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public String getPerformance() {
		return performance;
	}
	public void setPerformance(String performance) {
		this.performance = performance;
	}
	
	
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public MemberEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MemberEntity(Integer id, String openid, String username, String weixinno, String weixinimg, String telephone,
			String email, String sex, String birthday, String password, String rank_id) {
		super();
		this.id = id;
		this.openid = openid;
		this.username = username;
		this.weixinno = weixinno;
		this.weixinimg = weixinimg;
		this.telephone = telephone;
		this.email = email;
		this.sex = sex;
		this.birthday = birthday;
		this.password = password;
	}
	@Override
	public String toString() {
		return "MemberEntity [id=" + id + ", openid=" + openid + ", username=" + username + ", weixinno=" + weixinno
				+ ", weixinimg=" + weixinimg + ", telephone=" + telephone + ", email=" + email + ", sex=" + sex
				+ ", birthday=" + birthday + ", password=" + password + "]";
	}
	
}
