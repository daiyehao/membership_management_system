package com.naruto.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.naruto.dao.BaseMapper;
import com.naruto.entity.MemberEntity;
import com.naruto.response.BaseResponse;
import com.naruto.response.PointsResp;


@Service
public class RechargeService {

	@Autowired
	BaseMapper basicMapper;
	
	

	@Transactional
	public String operation_recharge_change(String id, double points, String operationType, String note) throws Exception {
		// select
		String sql_select = "select * from recharge where member_id= #{id}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql_select);
		paramMap.put("id", id);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		double amount = 0;
		// 取出查询的积分
		if (list != null && list.size() > 0) {
			System.out.println(list.get(0).get("rechargeAmount"));
			amount = Double.parseDouble(list.get(0).get("rechargeAmount")+"");
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("金额不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		if (operationType.equals("1")) {
			amount += points;
		} else {
			amount -= points;
			if (amount < 0) {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("金额修改失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		}

		// update
		String sql_update = "UPDATE `recharge` SET rechargeAmount=#{tatal} WHERE `member_id` = #{id};";
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put(BaseMapper.SQLFIELD, sql_update);
		paramMap2.put("tatal", amount);
		paramMap2.put("id", id);
		int lines = basicMapper.executeUpdate(paramMap2);
		if (lines > 0) {
			// INSERT INTO `academy`.`pointsexchange` (`id`, `member_id`, `card_id`,
			// `operationType`, `type`, `points`, `note`, `createtime`) VALUES (NULL, '1',
			// '1', '1', '', '100', 'add', NULL);
			String sql_add = "INSERT INTO " + "`academy`.`rechargerecord` "
					+ "(`member_id`, `operationType`, `rechargeAmount`, `note`)" + " VALUES"
					+ " (#{member_id},#{operationType}, #{rechargeAmount},#{note});";
			Map<String, Object> paramMap3 = new HashMap<String, Object>();
			paramMap3.put(BaseMapper.SQLFIELD, sql_add);
			paramMap3.put("member_id", id);
			paramMap3.put("operationType", operationType);
			paramMap3.put("rechargeAmount", points);
			paramMap3.put("note", note);
			int lines2 = basicMapper.executeUpdate(paramMap3);
			if (lines2 > 0) {
				PointsResp pointsResp = new PointsResp();
				pointsResp.setStatus("0");
				pointsResp.setResultMsg("充值成功！");
				String json = JSON.toJSONString(pointsResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("充值记录添加失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("充值失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}

	}



	public String operation_point_change_clear(String id, Double rechargeAmount, String operationType, String note) {
		// TODO Auto-generated method stub
		String sql_select = "select * from recharge where member_id= #{id}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put(BaseMapper.SQLFIELD, sql_select);
		paramMap.put("id", id);
		List<Map<String, Object>> list = basicMapper.fetch(paramMap);
		double amount = 0;
		// 取出查询的积分
		if (list != null && list.size() > 0) {
			System.out.println(list.get(0).get("rechargeAmount"));
			amount = Double.parseDouble(list.get(0).get("rechargeAmount")+"");
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("金额不存在！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
		amount = 0;
		// update
		String sql_update = "UPDATE `recharge` SET rechargeAmount=#{tatal} WHERE `member_id` = #{id};";
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put(BaseMapper.SQLFIELD, sql_update);
		paramMap2.put("tatal", amount);
		paramMap2.put("id", id);
		int lines = basicMapper.executeUpdate(paramMap2);
		if (lines > 0) {
			// INSERT INTO `academy`.`pointsexchange` (`id`, `member_id`, `card_id`,
			// `operationType`, `type`, `points`, `note`, `createtime`) VALUES (NULL, '1',
			// '1', '1', '', '100', 'add', NULL);
			String sql_add = "INSERT INTO " + "`academy`.`rechargerecord` "
					+ "(`member_id`, `operationType`, `rechargeAmount`, `note`)" + " VALUES"
					+ " (#{member_id},#{operationType}, #{rechargeAmount},#{note});";
			Map<String, Object> paramMap3 = new HashMap<String, Object>();
			paramMap3.put(BaseMapper.SQLFIELD, sql_add);
			paramMap3.put("member_id", id);
			paramMap3.put("operationType", operationType);
			paramMap3.put("rechargeAmount", rechargeAmount);
			paramMap3.put("note", note);
			int lines2 = basicMapper.executeUpdate(paramMap3);
			if (lines2 > 0) {
				PointsResp pointsResp = new PointsResp();
				pointsResp.setStatus("0");
				pointsResp.setResultMsg("清除成功！");
				String json = JSON.toJSONString(pointsResp);
				return json;
			} else {
				BaseResponse adminLoginResp = new BaseResponse();
				adminLoginResp.setStatus("-1");
				adminLoginResp.setResultMsg("清除记录添加失败！");
				String json = JSON.toJSONString(adminLoginResp);
				return json;
			}
		} else {
			BaseResponse adminLoginResp = new BaseResponse();
			adminLoginResp.setStatus("-1");
			adminLoginResp.setResultMsg("清除失败！");
			String json = JSON.toJSONString(adminLoginResp);
			return json;
		}
	}
}
