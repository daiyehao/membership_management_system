package com.naruto.response;

import java.io.Serializable;

public class BaseResponse<M> implements Serializable{

	private String status;
	private String resultMsg;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	@Override
	public String toString() {
		return "BaseResponse [status=" + status + ", resultMsg=" + resultMsg + "]";
	}
	
	
}
