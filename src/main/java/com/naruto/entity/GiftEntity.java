package com.naruto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.ibatis.type.JdbcType;

import tk.mybatis.mapper.annotation.ColumnType;

@Table(name = "gifts")
public class GiftEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ColumnType(jdbcType = JdbcType.BIGINT)
	private Integer id;
	@Column(name="giftsName")
	private String giftsName;
	private String giftstype;
	private String giftscount;
	private String exchangepoints;
	private String note;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public GiftEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getGiftsName() {
		return giftsName;
	}
	public void setGiftsName(String giftsName) {
		this.giftsName = giftsName;
	}
	public String getGiftstype() {
		return giftstype;
	}
	public void setGiftstype(String giftstype) {
		this.giftstype = giftstype;
	}
	public String getGiftscount() {
		return giftscount;
	}
	public void setGiftscount(String giftscount) {
		this.giftscount = giftscount;
	}
	public String getExchangepoints() {
		return exchangepoints;
	}
	public void setExchangepoints(String exchangepoints) {
		this.exchangepoints = exchangepoints;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Override
	public String toString() {
		return "GiftEntity [id=" + id + ", giftsName=" + giftsName + ", giftstype=" + giftstype + ", giftscount="
				+ giftscount + ", exchangepoints=" + exchangepoints + ", note=" + note + "]";
	}

	
}
