package com.naruto.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.jdbc.SQL;

import com.naruto.entity.AdminEntity;
import com.naruto.entity.School;


@Mapper
public interface MenuMapper  extends tk.mybatis.mapper.common.Mapper<AdminEntity>{

}
